﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week3
{
    class Dog
    {
        private int weight;
        private string name;
        private string color;

        public int GetWeight() { return weight; }
        public void SetWeight(int weight) { this.weight = weight; }

        public string GetName() { return name; }
        public void SetName(string name) { this.name = name; }

        public string GetColor() { return color; }
        public void SetColor(string color) { this.color = color; }

        public Dog(string name, int weight)
        {
            this.name = name;
            this.weight = weight;
            color = "Uninitialized";
        }

        public Dog() : this("Unknown", -1) {}

        public override string ToString()
        {
            //return "Name: " + name + " Weight: " + weight;
            //return string.Format("Name: {0}; Weight: {1}", name, weight);
            return $"Name: {name}; Weight: {weight}; Color: {color}";
        }
    }
}
