﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week6
{
    class Program
    {
        static void Main(string[] args)
        {
            DocumentTester();
        }

        private static void DocumentTester()
        {
            IStorable myVersion = new Version();
            IStorable myHeader = new Header();
            IStorable myBody = new Body();

            Document myDocument = new Document(myVersion, myHeader, myBody);
            myDocument.Write(myDocument);
            myDocument.Read();
        }
    }
}
