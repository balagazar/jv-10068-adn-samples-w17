﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week6
{
    class Document : IStorable
    {
        public string Path { get; set; }
        public string FileName { get; set; }
        public string Author { get; set; }
        public int Size { get; set; }

        private IStorable myVersion;
        private IStorable myHeader;
        private IStorable myBody;

        //private Version myVersion;
        //private Header myHeader;
        //private Body myBody;


        public Document()
        {
            myVersion = new Version();
            myHeader = new Header();
            myBody = new Body();
        }

        public Document(Version theVersion, Header theHeader, Body theBody)
        {
            myVersion = theVersion;
            myHeader = theHeader;
            myBody = theBody;
        }

        public Document(IStorable theVersion, IStorable theHeader, IStorable theBody)
        {
            myVersion = theVersion;
            myHeader = theHeader;
            myBody = theBody;
        }


        public int Status
        {
            get; set;
        }

        public bool Read()
        {
            Console.WriteLine("Document.Read");
            if (myVersion is IStorable)  // not necessary (always true) but for demonstration purposes
                myVersion.Read();
            myHeader.Read();
            myBody.Read();
            return true;
        }

        public bool Write(object o)
        {
            Console.WriteLine("Document.Write");
            if (o != null)
            {
                myVersion.Write(o);
                myHeader.Write(o);
                myBody.Write(o);
                return true;
            }
            else
                return false;
        }
    }
}
