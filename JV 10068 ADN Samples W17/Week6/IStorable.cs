﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week6
{
    interface IStorable
    {
        bool Read();
        bool Write(object o);
        int Status { get; set; }
    }
}
