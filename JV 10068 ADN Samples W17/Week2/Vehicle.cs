﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week2
{
    public abstract class Vehicle
    {
        private string seats;
        private string dash;
        private string engine;

        public void Accelerate()
        {
            Console.WriteLine("Vehicle:Accelerate");
        }
        public virtual void Decelerate()
        {
            Console.WriteLine("Vehicle:Decelerate");
        }
        public virtual void Turn()
        {
            Console.WriteLine("Vehicle:Turn");
        }
        public abstract void Maintain();
    }
}
