﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week4
{
    class Program
    {
        static void Main(string[] args)
        {
            //PropertyTester();
            //VehicleTester();
            ControlTester();
        }

        private static void ControlTester()
        {
            // Can't instantiate new Controls since Control is abstract
            //Control myControl = new Control();

            // Control Constructor not being invoked so this is legal
            Control[] myControls1 = new Control[3];

            // Most common use of Control class - as a reference to array/collection of derived classes
            Control[] myControls = {new Button(),
                                    new ListBox(),
                                    new Button(),
                                    new ListBox()};
            
            // polymorphism in action - the Draw method of the referenced object's type is invoked
            // ie. Button.Draw and ListBox.Draw
            foreach (Control myControl in myControls)
                myControl.Draw();
        }

        private static void VehicleTester()
        {
            Vehicle[] myVehicles = {new Car(),
                                    new Plane(),
                                    new Car(),
                                    new Plane() };

            foreach (Vehicle vehicle in myVehicles)
            {
                vehicle.Maintain();
                vehicle.Accelerate();

                //((Car)vehicle).CarOnlyMethod();
                if (vehicle is Car)
                   ((Car)vehicle).CarOnlyMethod();

                Car myCar = vehicle as Car;
                if (myCar != null)
                    myCar.CarOnlyMethod();
            }
        }

        private static void PropertyTester()
        {
            Dog oldSchoolDog = new Dog();
            oldSchoolDog.SetName("Duke");
            oldSchoolDog.SetWeight(10);
            oldSchoolDog.SetColor("White");
            Console.WriteLine("Name: " + oldSchoolDog.GetName() + ", Weight: " + oldSchoolDog.GetWeight());

            DogWithProperties newSchoolDog = new DogWithProperties("Duke");
            //newSchoolDog.Name = "Duke";
            newSchoolDog.Weight = 10;
            newSchoolDog.Color = "Black";
            Console.WriteLine("Name: " + newSchoolDog.Name + ", Weight: " + newSchoolDog.Weight);
        }
    }
}
