﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week4
{
    abstract class Control
    {
        public int Top { get; set; }
        public int Left { get; set; }

        public Control() { }
        public abstract void Draw();
    }
}
